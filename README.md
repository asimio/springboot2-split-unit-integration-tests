# README #

Accompanying source code for blog entry at https://tech.asimio.net/2019/04/08/Splitting-Unit-and-Integration-Tests-using-Maven-and-Surefire-plugin.html

### Requirements ###

* Java 7+
* Maven 3.2+

### Building and running from command line ###

```
mvn clean verify
```

### Who do I talk to? ###

* orlando.otero at asimio dot net
* https://www.linkedin.com/in/ootero